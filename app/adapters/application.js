import DS from 'ember-data';
import { pluralize } from 'ember-inflector';
 
var Adapater=DS.RESTAdapter.extend({
  ajaxOptions: function(url, type, options) {
    var hash = this._super(url, type, options);
    if (type == 'POST' || type=='PUT') {
      hash.dataType = 'text';
    }
    return hash;
  },
  host:'http://127.0.0.1:8080',
  namespace: 'script_librabry',
  urlForDeleteRecord(id, modelName) {
    modelName=pluralize(modelName);
    return this.get('host')+'/'+this.get('namespace')+`/${modelName}/*?filter={_id:'${id}'}`;
  }/*,
  query(store, modelName, query) {
    modelName=pluralize(modelName.modelName);
    let url = this.get('host')+'/'+this.get('namespace')+'/'+modelName+'?filter='+JSON.stringify(query);
    if (this.sortQueryParams) {
      query = this.sortQueryParams(query);
    }
    return this.ajax(url, 'GET');
  }*/
  ,
  urlForQuery(query,modelName) {
  if (this.sortQueryParams) {
    query = this.sortQueryParams(query);
  }
  modelName=pluralize(modelName);
    return this.get('host')+'/'+this.get('namespace')+'/'+modelName+'?filter='+JSON.stringify(query.filter);
  }
});
 
export default Adapater;