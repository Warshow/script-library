import DS from 'ember-data';

export default DS.Model.extend({
    // attributs des scripts
    name:DS.attr('string'),
    description:DS.attr('string'),
    content:DS.attr('string'),

    // relation avec d'autre model
    categorys:DS.belongsTo('category'),
    typeScript:DS.belongsTo('type'),
    languages:DS.belongsTo('language'),
    proprio:DS.belongsTo('user'),
    //groups:DS.hasMany('group'),
    groups:DS.hasMany('rightgroup'),
    locations:DS.hasMany('location'),
    historys:DS.hasMany('history')
    
});
