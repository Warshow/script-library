import DS from 'ember-data';

export default DS.Model.extend({
    // attributs
    login:DS.attr('string'),
    password:DS.attr('string'),
    email:DS.attr('string'),
    identity:DS.attr('string'),
    
    // relations
    scrypts:DS.hasMany('script'),
    groups:DS.hasMany('group',{inverse:null})
});
