import DS from 'ember-data';

export default DS.Model.extend({
    // attributs
    name:DS.attr('string'),
    // relations
    scripts:DS.hasMany('script')
});
