import DS from 'ember-data';

export default DS.Model.extend({
    write:DS.attr('boolean'),
    read:DS.attr('boolean')
});
