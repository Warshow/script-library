import DS from 'ember-data';

export default DS.Model.extend({
    group:DS.belongsTo('group'),
    script:DS.belongsTo('script'),
    write:DS.attr('boolean',{defaultValue:false}),
    read:DS.attr('boolean',{defaultValue:false})
});