import DS from 'ember-data';

export default DS.Model.extend({
    date: DS.attr('utc'),
    content: DS.attr('string'),
    hscript:DS.belongsTo('script')
});
