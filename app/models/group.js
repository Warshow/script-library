import DS from 'ember-data';

export default DS.Model.extend({
    name:DS.attr('string'),
    proprio:DS.belongsTo('user'),
    users:DS.hasMany('user'),
    //scripts:DS.hasMany('script')
    script:DS.hasMany('rightgroup')
});
