import Route from '@ember/routing/route';
import RSVP from 'rsvp';


export default Route.extend({
    model(){
        return RSVP.hash({
            OwnerG : this.store.query('group',{filter: {proprio:sessionStorage.getItem("iduser")}}),
            UserG : this.store.query('group',{filter:{users:sessionStorage.getItem("iduser")}})
        }) 
    }
});
