import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
    
    model(param){
        return RSVP.hash({
            right : '',
            group: this.store.findRecord('group',param.group_id,{include:'proprio,users,scripts'})
        })
        
        
    }
    
});
