import Route from '@ember/routing/route';

export default Route.extend({
    templateName: 'scripts',
    model(param){
        return this.store.query('script', {filter:{languages:param.language_id}})
    }
});
