import Route from '@ember/routing/route';
import $ from 'jquery';
import RSVP from 'rsvp';

export default Route.extend({
  actions: {
    openModal: function () {
      $('#test-modal').modal('show')
    },
    connModal: function (){
      $('#LoginU').modal('show')
    },
    Modalgroup(){
      $('#Cgroup').modal('show')
    
    },
    modMineG(){
      $('#MineG').modal('show')
    },
    ShowProfile(){
      $('#modalProfil').modal('show')
    }
   
  
},
model(){
  return RSVP.hash({
    categorys : this.store.findAll('category'),
    types: this.store.findAll('type'),
    groups : null,
    minescripts : null,
    locations : this.store.findAll('location'),
    languages: this.store.findAll('language'),
    user: null//this.store.findRecord('user','6051bebb-3ce2-48ef-b6f9-bc3128fe3d7f')
  })
}
});
