import Route from '@ember/routing/route';
import RSVP from 'rsvp';



export default Route.extend({
    model(params){
       return RSVP.hash({
         categorys : this.store.findAll('category'),
         types : this.store.findAll('type'),
         script : this.store.findRecord('script',params.script_id, { include: 'typeScript,catgorys,languages'}),
         languages: this.store.findAll('language')
       })
       
    },
    actions:{
        save(script){
         
          let cate = this.store.peekRecord('category', document.querySelector('#Scatego').value)
             let typ = this.store.peekRecord('type', document.querySelector('#Stype').value)
             let lang = this.store.peekRecord('language', document.querySelector('#Slang').value)
             
            script.set('name',script.name);
            script.set('categorys',cate);
            script.set('typeScript',typ);
            script.set('languages',lang);
            script.set('description',script.description);
            script.set('content',script.content);
          script.save().then(()=>{
            this.transitionTo('scripts');
          });
        },
        delete(script){
                script.deleteRecord();
                script.get('isDeleted'); // => true
                script.save().then(()=>{
                  this.transitionTo('scripts');
              });
        }
    }
    
});
