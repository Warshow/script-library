import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        filterByName(param) {
          let idType= window.location.pathname.split('/')
          if (param !== '') {
            var request= new RegExp("^"+param+".*")
           let p = this.store.query('script', {filter:{'name':{'$regex':'(?i)'+request.toString().substring(1,request.toString().length-1)},typeScript: idType[idType.length-1]}})
           ////////////////////////////////////////// {'name':{'$regex':'(?i)^'+param+'.*'}}
           this.set('model',p)
            
          } else {
            let o=this.store.query('script', {filter:{typeScript: idType[idType.length-1]}})
            this.set('model',o);
          } 
        } 
       }
});
