import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        filterByName(param) {
            let OwnerG;
            let UserG;
          if (param !== '') {
            var request= new RegExp("^"+param+".*")
            OwnerG = this.store.query('group', {filter:{'name':{'$regex':'(?i)'+request.toString().substring(1,request.toString().length-1)},proprio:sessionStorage.getItem("iduser")}})
            UserG = this.store.query('group', {filter:{'name':{'$regex':'(?i)'+request.toString().substring(1,request.toString().length-1)},users:sessionStorage.getItem("iduser")}})
           ////////////////////////////////////////// {'name':{'$regex':'(?i)^'+param+'.*'}}
           this.set('model.OwnerG',OwnerG)
           this.set('model.UserG',UserG)
            
          } else {
            OwnerG = this.store.query('group',{filter: {proprio:sessionStorage.getItem("iduser")}})
            UserG = this.store.query('group',{filter:{users:sessionStorage.getItem("iduser")}})
            this.set('model.OwnerG',OwnerG)
           this.set('model.UserG',UserG)
          } 
        } 
       } 
});
