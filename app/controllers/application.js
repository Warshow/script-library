import Controller from '@ember/controller';
import $ from 'jquery';
import { set } from '@ember/object';






export default Controller.extend({
    actions: {
        selectOption: function () {
            let elemValue=document.querySelector('#selecto').value
            if (elemValue=="script") {
                this.toggleProperty('isScript')
                console.log()
            }else{
                this.toggleProperty('')
            }
          },
          save: function(entity) {  // entity contient les données fournit dans les champs

            // enregistre la valeur selctionner dans le select
             let elem=document.querySelector('#selecto').value
            // si la valeur est égale à la string "script"
             if (elem=="script") {
                 
                // on va chercher les valeurs des select pour les futur relations
                let cate = this.store.peekRecord('category', document.querySelector('#Scatego').value)
                let typ = this.store.peekRecord('type', document.querySelector('#Stype').value)
                let lang = this.store.peekRecord('language', document.querySelector('#Slang').value)
                let idU = this.store.peekRecord('user', sessionStorage.getItem("iduser"))
   
                //console.log(cate,typ,lang,idU)
              

                // on crée un script dans lequel on attribue la valeur corresondante à chaque attribut 
               entity=this.store.createRecord('script',{
                   proprio: idU,                // id du propriétaire
                   name : entity.name,              // nom du script
                   description: entity.description,     // description du script
                   content: entity.content,         // le script lui même
                   categorys: cate,                 // l'id de la catégorie du script
                   typeScript: typ,                 // l'id du type du script
                   languages: lang                  // l'id di langage du script
               })
             }else{     // si la chaine sélectionner n'ai pas égal à "script"
                 entity=this.store.createRecord(elem,entity)  // alors on crée simplement l'entité
             }
            
        entity.save().then(()=>{        // on save la création pour l'envoyer à la base de données
            console.log('save');
        })


          },
          NewUser: function(newu){
              newu=this.store.createRecord('user',newu)
              newu.save().then(()=>{
                  this.transitionTo('application')
                  console.log('save new user');
              })
          },
          LoginU:function(connu){
             
                // Requète qui charge l'user en fonction du login et mdp
                this.store.query('user',{filter : { login : connu.login, password : connu.password}}).then((data)=>{
                  console.log(data.objectAt(0))
                // Création d'un objet user
                let user = { 
                    "iduser" : data.objectAt(0).id,
                    "login" : data.objectAt(0).login,
                    "email" : data.objectAt(0).email,
                    "identity" : data.objectAt(0).identity 
                }
                // Set le model user avec le l'objet user
                this.set('model.user',user) 

                // On crée la variable de session
                sessionStorage.setItem("iduser",this.get('model.user.iduser'))
                sessionStorage.setItem("login",this.get('model.user.login'))
                sessionStorage.setItem("email",this.get('model.user.email'))
                sessionStorage.setItem("identity",this.get('model.user.identity'))
                
                // cache le bouton login
                $('#Bconn').css("display", "none")
                $('#Disco').removeClass('transition hidden')
                $('#Profile').removeClass('transition hidden')
                
                // On affiche l'infos sur la div portant l'id "DivC" dans la page (a la place du bouton login)
                document.querySelector('#DivC').innerHTML = sessionStorage.getItem("identity")



                let groups = this.store.query('group',{filter: {proprio:sessionStorage.getItem("iduser")},include:'proprio'})
                this.set('model.groups',groups)

                let mineScripts= this.store.query('script',{filter:{proprio:sessionStorage.getItem("iduser")},include:'proprio,languages'})
                this.set('model.minescripts',mineScripts)
               
                let users= this.store.findAll('user')
                this.set('model.allusers',users)
               })      

          },
          deco(){
            sessionStorage.clear()
            document.location.reload(true)
            this.transitionTo('')
          },
          editProfile(user){
            let Puser=this.store.peekRecord('user',user.iduser)
            Puser.set('email',user.email)
            Puser.set('login',user.login)
            Puser.set('identity',user.identity)
            Puser.save().then(()=>{
              this.set('model.user',Puser);
              sessionStorage.setItem("login",this.get('model.user.login'))
                sessionStorage.setItem("email",this.get('model.user.email'))
                sessionStorage.setItem("identity",this.get('model.user.identity'))
                document.querySelector('#DivC').innerHTML = sessionStorage.getItem("identity")
            });
           
            
          },
          CreateGroup(namegroup){
            let idU = this.store.peekRecord('user', sessionStorage.getItem("iduser"))
           let gr = this.store.createRecord('group',{
                name: namegroup,
                proprio: idU
            })
            gr.save().then(()=>{        // on save la création pour l'envoyer à la base de données
            console.log('group saved');
        })
          },
          linkSG(){
              let idL=""
              let idU=""
              let idS=""
              let idG=""
              let row=""
              var that=this.store
              var Us=[]
              var Sc=[]
            

              /*$("#tabLoc input[type=checkbox]:checked").each(function () {
                row = $(this).closest("tr")[0];
               idL = row.cells[0].innerHTML;
              Lo.push(that.peekRecord('location',idL))
           });*/
           $("#tabScript input[type=checkbox]:checked").each(function () {
            row = $(this).closest("tr")[0];
           idS = row.cells[0].innerHTML;
           Sc.push(that.peekRecord('script',idS))
       });

           $("#tabLoc input[type=checkbox]:checked").each(function () {
            row = $(this).closest("tr")[0];
            idL = row.cells[0].innerHTML;
          var Loc = that.peekRecord('location',idL)
          Sc.forEach(function(elem){
            Loc.get("scripts").pushObject(elem);
            Loc.save();
           })
           Sc=[]
       });

              $("#tabUser input[type=checkbox]:checked").each(function () {
                row = $(this).closest("tr")[0];
               idU = row.cells[0].innerHTML;
              Us.push(that.peekRecord('user',idU))
           });
           $("#tabGroup input[type=checkbox]:checked").each(function () {
            row = $(this).closest("tr")[0];
           idG = row.cells[0].innerHTML;
          var Gro = that.peekRecord('group',idG)
           Us.forEach(function(elem){
             if(elem.id !== sessionStorage.getItem('iduser')){
              Gro.get("users").pushObject(elem);
              Gro.save();
             }
           })
       });
            $("#tabScript input[type=checkbox]:checked").each(function () {
              row = $(this).closest("tr")[0];
             idS = row.cells[0].innerHTML;
             Sc.push(that.peekRecord('script',idS))
         });
            
         $("#tabGroup input[type=checkbox]:checked").each(function () {
          row = $(this).closest("tr")[0];
         idG = row.cells[0].innerHTML;
        var Gr = that.peekRecord('group',idG)
         Sc.forEach(function(elem){
          var rightGS = that.createRecord('rightgroup',{
            group: Gr,
            script: elem
          })
          rightGS.save()
         }) 
     });

         /*$("#tabGroup input[type=checkbox]:checked").each(function () {
          row = $(this).closest("tr")[0];
         idG = row.cells[0].innerHTML;
        var Gr = stor.peekRecord('group',idG)
         Sc.forEach(function(elem){
             Gr.get("scripts").pushObject(elem);
             Gr.save();
         }) 
     });*/

            
          },
          MenuI(){
    
            $('#tabS').on('click',function(){
              $(this).addClass('active');
              $('#tabU').removeClass('active');
              $('#tabL').removeClass('active');
              $('#tabUser').addClass('transition hidden');
              $('#tabLoc').addClass('transition hidden');
              $('#tabScript').removeClass('transition hidden');
              });
          
            $('#tabU').on('click',function(){
              $(this).addClass('active');
              $('#tabS').removeClass('active');
              $('#tabL').removeClass('active');
              $('#tabScript').addClass('transition hidden');
              $('#tabLoc').addClass('transition hidden');
              $('#tabUser').removeClass('transition hidden');
            })

            $('#tabL').on('click',function(){
              $(this).addClass('active');
              $('#tabS').removeClass('active');
              $('#tabU').removeClass('active');
              $('#tabScript').addClass('transition hidden');
              $('#tabUser').addClass('transition hidden');
              $('#tabLoc').removeClass('transition hidden');
            })
          }
          
          
          /*,
          tlog(){
            console.log('fgjfg')
              if (sessionStorage.getItem("iduser")==undefined||sessionStorage.getItem("iduser")==null) {
                  this.send('connModal')
                  console.log('fgjfg')
              }else{

                  let user = {
                    "iduser" : sessionStorage.getItem("iduser"),
                    "login" : sessionStorage.getItem("login"),
                    "email" : sessionStorage.getItem("email"),
                    "identity" : sessionStorage.getItem("identity")
                  }
                  this.set("model.user",user)
              }
          },
          didRender(){
            $(window).on('load', this.actions('tlog'))
            console.log('fuhziuhf')
          }*/

}
});
