import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        filterByName(param) {
            
          if (param !== '') {
            var request= new RegExp("^"+param+".*")
           let p = this.store.query('location', {filter:{'name':{'$regex':'(?i)'+request.toString().substring(1,request.toString().length-1)}} })
           ////////////////////////////////////////// {'name':{'$regex':'(?i)^'+param+'.*'}}
           this.set('model',p)
            
          } else {
            let o=this.store.findAll('location')
            this.set('model',o);
          } 
        } 
       } 
});
