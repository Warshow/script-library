import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('users');
  this.route('scripts', function() {
    this.route('edit',{path : 'edit/:script_id'});
  });
  this.route('categorys', function() {
    this.route('children_cate',{path : 'children_cate/:category_id'});
  });
  this.route('types', function() {
    this.route('children-type',{path : 'children-type/:type_id'});
  });
  this.route('languages', function() {
    this.route('children-lang',{path : 'children-lang/:language_id'});
  });
  this.route('group-page',{path : 'group-page/:group_id'});
  this.route('my-groups');
  this.route('locations');
});

export default Router;
