import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | types/children-type', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:types/children-type');
    assert.ok(controller);
  });
});
