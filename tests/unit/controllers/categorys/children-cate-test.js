import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | categorys/children-cate', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:categorys/children-cate');
    assert.ok(controller);
  });
});
