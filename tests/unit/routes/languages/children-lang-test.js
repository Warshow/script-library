import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | languages/children-lang', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:languages/children-lang');
    assert.ok(route);
  });
});
