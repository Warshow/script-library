import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | types/children-type', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:types/children-type');
    assert.ok(route);
  });
});
