import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | categorys', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:categorys');
    assert.ok(route);
  });
});
